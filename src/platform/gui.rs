use std::collections::VecDeque;

use piston::{Button, ButtonArgs, ButtonEvent, ButtonState, RenderEvent};
use piston_window::types::Rectangle;
use piston_window::{
    G2dTexture, G2dTextureContext, PistonWindow, Texture, TextureSettings, WindowSettings,
};

use crate::{
    input::{GameCommand, InputAction},
    map::MapTile,
    movement::MoveDirection,
};

use super::PlatformAPI;

pub struct GameWindow {
    window: PistonWindow,
    atlas: Atlas,
    last_render_event: Option<piston::Event>,
    event_buffer: VecDeque<InputAction>,
}

impl GameWindow {
    pub fn new() -> Self {
        let mut window: PistonWindow = WindowSettings::new("Hello Piston!", [640, 480])
            .build()
            .unwrap();
        let mut ctx = window.create_texture_context();
        let atlas = Atlas::load(&mut ctx);

        Self {
            window,
            atlas,
            event_buffer: Default::default(),
            last_render_event: None,
        }
    }

    pub(crate) fn drain_events(&mut self) {
        for event in &mut self.window {
            if event.render_args().is_some() {
                self.last_render_event = Some(event);
            } else if let Some(ButtonArgs {
                state: ButtonState::Press,
                button: Button::Keyboard(key),
                ..
            }) = event.button_args()
            {
                use piston::Key;
                use InputAction::*;

                let command = match key {
                    Key::Left => Movement(MoveDirection::Left),
                    Key::Right => Movement(MoveDirection::Right),
                    Key::Up => Movement(MoveDirection::Up),
                    Key::Down => Movement(MoveDirection::Down),

                    Key::Q | Key::Escape => Control(GameCommand::Quit),
                    Key::N => Control(GameCommand::NextMap),
                    Key::P => Control(GameCommand::PreviousMap),
                    Key::R => Control(GameCommand::Reset),
                    _ => continue,
                };
                self.event_buffer.push_back(command);
            }
        }
    }
}

/// todo: add button usage text

impl crate::gfx::Draw for GameWindow {
    fn draw(&mut self, map: &crate::map::Map) {
        use piston_window::*;

        self.drain_events();

        let Some(event) = &self.last_render_event else { return };

        let atlas = &mut self.atlas;
        self.window.draw_2d(event, |ctx, g, _device| {
            clear([1.0; 4], g);

            let tile: MapTile = todo!();
            let source_rectangle = Atlas::source_rect_from_tile(tile);
            let target = source_rectangle;
            target[0] = todo!();
            target[1] = todo!();
            Image {
                color: None,
                source_rectangle: Some(source_rectangle),
                rectangle: Some(target),
            }
            .draw(&atlas.inner, &Default::default(), ctx.transform, g);
        });
    }
}

impl crate::input::InputProvider for GameWindow {
    fn next_input_action(&mut self) -> Option<InputAction> {
        self.event_buffer.pop_front()
    }
}

use crate::platform::assets::EmbedMapLoader;

impl PlatformAPI for GameWindow {
    fn map_loader(&self) -> Box<dyn crate::map::MapLoader> {
        Box::<EmbedMapLoader>::default()
    }

    fn renderer(&mut self) -> &mut dyn crate::gfx::Draw {
        self
    }

    fn input_provider(&mut self) -> &mut dyn crate::input::InputProvider {
        self
    }
}

struct Atlas {
    inner: G2dTexture,
}

impl Atlas {
    fn load(ctx: &mut G2dTextureContext) -> Atlas {
        let img = image::load_from_memory(include_bytes!("../assets/atlas.png")).unwrap();
        Self {
            inner: Texture::from_image(ctx, &img.into_rgba8(), &TextureSettings::new()).unwrap(),
        }
    }

    // tile size 5 x 5
    //
    // from left to right:
    // Space,
    // Block,
    // TargetZone,
    // Block + TargetZone,
    // Wall,
    fn source_rect_from_tile(tile: MapTile) -> Rectangle {
        use MapTile::*;
        let x = match tile {
            Space => 0,
            Block => 1,
            TargetZone => 2,
            Wall => 4,
        };
        [5.0 * x as f64, 0.0, 5., 5.]
    }
}
