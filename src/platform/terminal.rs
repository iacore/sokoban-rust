use std::io;
use std::io::Write;

use crate::gfx::Draw;
use crate::input::*;
use crate::map::*;
use crate::movement::MoveDirection;

use termion::event::Key;
use termion::input::TermRead;
use termion::raw::IntoRawMode;

pub struct TerminalDrawer {
    pub(crate) stdout: termion::raw::RawTerminal<io::Stdout>,
}

impl TerminalDrawer {
    pub fn new(stdout: io::Stdout) -> TerminalDrawer {
        let stdout = stdout.into_raw_mode().unwrap();
        TerminalDrawer { stdout }
    }

    pub(crate) fn draw_help_text(&mut self, map: &Map) {
        write!(
            self.stdout,
            "Map {}\r\nq - quit, r - reset, n - next map, p - previous map\r\n",
            map.id
        )
        .unwrap();
    }
}

impl Drop for TerminalDrawer {
    fn drop(&mut self) {
        self.stdout.suspend_raw_mode().unwrap();
    }
}

impl Draw for TerminalDrawer {
    fn draw(&mut self, map: &Map) {
        write!(self.stdout, "{}", termion::clear::All).unwrap();
        let mut map_dim = 0;
        for y in 0..map.map.len() {
            let mut current_line = String::new();
            for x in 0..map.map[y].len() {
                match map.map[y][x] {
                    MapTile::Wall => current_line += "X",
                    MapTile::TargetZone => current_line += ".",
                    _ => current_line += " ",
                }
            }
            writeln!(
                self.stdout,
                "{}{}",
                termion::cursor::Goto(1, (y + 1) as u16),
                current_line
            )
            .unwrap();
            if current_line.trim().is_empty() {
                map_dim = y;
                break;
            }
        }
        writeln!(
            self.stdout,
            "{}@",
            termion::cursor::Goto(
                (map.player_position.x + 1) as u16,
                (map.player_position.y + 1) as u16
            )
        )
        .unwrap();
        for block in map.movable_blocks.iter() {
            writeln!(
                self.stdout,
                "{}*",
                termion::cursor::Goto((block.position.x + 1) as u16, (block.position.y + 1) as u16)
            )
            .unwrap();
        }
        write!(
            self.stdout,
            "{}",
            termion::cursor::Goto(0, (map_dim + 2) as u16)
        )
        .unwrap();

        self.draw_help_text(map);
    }
}

#[cfg(test)]
pub(crate) mod tests_draw {
    use super::*;
    use std::io::stdout;
    use termion::raw::IntoRawMode;

    #[test]
    fn test_drawing_can_be_instatiated() {
        let stdout_for_drawing = stdout().into_raw_mode().unwrap();
        let _drawing_module = TerminalDrawer {
            stdout: stdout_for_drawing,
        };
        stdout()
            .into_raw_mode()
            .unwrap()
            .suspend_raw_mode()
            .unwrap_or(());
    }
}

pub struct TerminalInputProvider {
    pub(crate) stdin: Box<(dyn Iterator<Item = io::Result<Key>>)>,
}

impl InputProvider for TerminalInputProvider {
    fn next_input_action(&mut self) -> Option<InputAction> {
        use InputAction::*;

        let Some(c) = self.stdin.next() else {
            // quit of eof
            return Some(Control(GameCommand::Quit))
        };
        let c = c.unwrap();

        let command = match c {
            Key::Left => Movement(MoveDirection::Left),
            Key::Right => Movement(MoveDirection::Right),
            Key::Up => Movement(MoveDirection::Up),
            Key::Down => Movement(MoveDirection::Down),

            Key::Char('q') | Key::Esc => Control(GameCommand::Quit),
            Key::Char('n') => Control(GameCommand::NextMap),
            Key::Char('p') => Control(GameCommand::PreviousMap),
            Key::Char('r') => Control(GameCommand::Reset),
            _ => return None,
        };

        Some(command)
    }
}

impl TerminalInputProvider {
    pub fn new() -> TerminalInputProvider {
        TerminalInputProvider {
            stdin: Box::new(io::stdin().keys()),
        }
    }
}

#[cfg(test)]
pub(crate) mod tests_input {
    use super::*;

    pub struct FakeUserInputProvider {
        pub key: Key,
    }

    impl Iterator for FakeUserInputProvider {
        type Item = io::Result<Key>;
        fn next(&mut self) -> Option<Self::Item> {
            Some(Ok(self.key))
        }
    }

    #[test]
    fn test_input() {
        let fake_input_provider = FakeUserInputProvider {
            key: Key::Char('q'),
        };
        let mut input_provider = TerminalInputProvider {
            stdin: Box::new(fake_input_provider),
        };
        assert!(!matches!(
            input_provider.next_input_action(),
            Some(InputAction::Movement(_))
        ));
    }

    macro_rules! movement_input_tests {
        ($($name:ident: $value:expr,)*) => {
        $(
            #[test]
            fn $name() {
                let (action, key) = $value;
                let fake_input_provider = FakeUserInputProvider {
                    key
                };
                let mut input_provider = TerminalInputProvider{stdin: Box::new(fake_input_provider)};
                assert_eq!(
                    Some(action),
                    input_provider.next_input_action()
                );
            }
        )*
        }
    }

    movement_input_tests! {
        test_up: (InputAction::Movement(MoveDirection::Up), Key::Up),
        test_down: (InputAction::Movement(MoveDirection::Down), Key::Down),
        test_left: (InputAction::Movement(MoveDirection::Left), Key::Left),
        test_right: (InputAction::Movement(MoveDirection::Right), Key::Right),
        test_q: (InputAction::Control(GameCommand::Quit), Key::Char('q')),
        test_p: (InputAction::Control(GameCommand::PreviousMap), Key::Char('p')),
        test_n: (InputAction::Control(GameCommand::NextMap), Key::Char('n')),
        test_r: (InputAction::Control(GameCommand::Reset), Key::Char('r')),
    }
}

use super::PlatformAPI;
use crate::platform::assets::EmbedMapLoader;
use std::io::stdout;

pub struct TerminalPlatformAPI {
    renderer: TerminalDrawer,
    input_provider: TerminalInputProvider,
}
impl TerminalPlatformAPI {
    pub fn default() -> Self {
        Self {
            renderer: TerminalDrawer::new(stdout()),
            input_provider: TerminalInputProvider::new(),
        }
    }
}
impl PlatformAPI for TerminalPlatformAPI {
    fn map_loader(&self) -> Box<dyn MapLoader> {
        Box::<EmbedMapLoader>::default()
    }

    fn renderer(&mut self) -> &mut dyn Draw {
        &mut self.renderer
    }

    fn input_provider(&mut self) -> &mut dyn InputProvider {
        &mut self.input_provider
    }
}
