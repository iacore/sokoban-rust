#[cfg(feature = "gui")]
mod gui;
#[cfg(feature = "terminal")]
mod terminal;

pub mod assets;

use crate::gfx::Draw;
use crate::input::InputProvider;
use crate::map::MapLoader;

pub trait PlatformAPI {
    fn map_loader(&self) -> Box<dyn MapLoader>;
    fn renderer(&mut self) -> &mut dyn Draw;
    fn input_provider(&mut self) -> &mut dyn InputProvider;
}

#[cfg(feature = "terminal")]
pub fn default_platform() -> Box<dyn PlatformAPI> {
    Box::new(terminal::TerminalPlatformAPI::default())
}

#[cfg(feature = "gui")]
pub fn default_platform() -> Box<dyn PlatformAPI> {
    Box::new(gui::GameWindow::new())
}
