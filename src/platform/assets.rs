use std::io;

use crate::map::MapLoader;

#[derive(Default)]
pub struct EmbedMapLoader {}

impl MapLoader for EmbedMapLoader {
    fn get_maps(&self) -> Result<String, io::Error> {
        Ok(include_str!("../assets/maps.txt").to_owned())
    }
}
