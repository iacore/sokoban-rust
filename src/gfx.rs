use crate::map::Map;

pub trait Draw {
    fn draw(&mut self, map: &Map);
}
