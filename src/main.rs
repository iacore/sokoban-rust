pub mod game;
pub mod gfx;
pub mod input;
pub mod map;
pub mod movement;
pub mod platform;

use std::io;

use game::Game;

fn main() -> Result<(), io::Error> {
    let mut platform = platform::default_platform();
    let mut game = Game::default();
    game.map_manager.load_maps(&mut *platform.map_loader())?;
    game.main_loop(&mut *platform);
    Ok(())
}
