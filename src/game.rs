use crate::gfx::Draw;
use crate::input::{GameCommand, InputAction};
use crate::map::*;
use crate::movement::{self, MoveDirection};
use crate::platform::PlatformAPI;

#[derive(Default)]
pub struct Game {
    pub map_manager: MapManager,
    pub current_map_id: usize,
}

impl Game {
    fn get_current_map(&self) -> Map {
        self.map_manager.maps[self.current_map_id].clone()
    }

    pub fn main_loop(&mut self, platform: &mut dyn PlatformAPI) {
        while let Some(cmd) = self.input_loop(platform) {
            match cmd {
                GameCommand::Quit => break,
                GameCommand::NextMap => {
                    if self.current_map_id + 1 < self.map_manager.maps.len() {
                        self.current_map_id += 1
                    }
                }
                GameCommand::PreviousMap => {
                    if self.current_map_id > 0 {
                        self.current_map_id -= 1;
                    }
                }
                _ => (),
            }
        }
    }

    fn render(&self, drawer: &mut dyn Draw, map: &Map) {
        drawer.draw(map);
    }

    fn input_loop(&self, platform: &mut dyn PlatformAPI) -> Option<GameCommand> {
        use InputAction::*;

        let mut current_map = self.get_current_map();
        self.render(platform.renderer(), &current_map);

        while let Some(user_input) = platform.input_provider().next_input_action() {
            match user_input {
                Movement(movedir) => {
                    self.handle_movement(&mut current_map, movedir);
                    self.render(platform.renderer(), &current_map);
                    if self.check_has_won(&current_map) {
                        return Some(GameCommand::NextMap);
                    }
                }
                Control(cmd) => return Some(cmd),
            }
        }
        None
    }

    fn handle_movement(&self, current_map: &mut Map, movedir: MoveDirection) {
        let new_pos =
            movement::calc_new_position_after_movement(&movedir, &current_map.player_position);
        if movement::can_move_to(current_map, &new_pos, &movedir, false) {
            if let Some(block) = current_map.get_movable_block_at(&new_pos) {
                let new_pos_block = movement::calc_new_position_after_movement(&movedir, &new_pos);
                block.move_to(&movedir);
                self.calc_nof_blocks_in_target_position(current_map, &new_pos, &new_pos_block);
            }
            current_map.player_position = new_pos;
        }
    }

    fn calc_nof_blocks_in_target_position(
        &self,
        map: &mut Map,
        old_position: &Position,
        new_position: &Position,
    ) {
        if map.get_tile_type_for_position(new_position) == MapTile::TargetZone
            && map.get_tile_type_for_position(old_position) != MapTile::TargetZone
        {
            map.movable_blocks_in_final_position += 1;
        }

        if map.get_tile_type_for_position(new_position) != MapTile::TargetZone
            && map.get_tile_type_for_position(old_position) == MapTile::TargetZone
        {
            map.movable_blocks_in_final_position -= 1;
        }
    }

    fn check_has_won(&self, map: &Map) -> bool {
        if map.movable_blocks_in_final_position == map.movable_blocks.len() {
            return true;
        }
        false
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::map::{MapTile, MovableBlock, Position};
    fn setup_tests() -> (Game, Map) {
        let mut game = Game::default();
        let mut map = Map::default();
        map.map[0][0] = MapTile::TargetZone;
        game.map_manager.maps.push(map.clone());
        (game, map)
    }

    #[test]
    fn test_move_box_into_target_zone() {
        let (game, mut map) = setup_tests();
        let old_position = Position { x: 1, y: 0 };
        let new_position = Position { x: 0, y: 0 };
        game.calc_nof_blocks_in_target_position(&mut map, &old_position, &new_position);
        assert_eq!(1, map.movable_blocks_in_final_position);
    }

    #[test]
    fn test_move_box_out_of_target_zone() {
        let (game, mut map) = setup_tests();
        let old_position = Position { x: 0, y: 0 };
        let new_position = Position { x: 1, y: 0 };
        map.movable_blocks_in_final_position = 1;
        game.calc_nof_blocks_in_target_position(&mut map, &old_position, &new_position);
        assert_eq!(0, map.movable_blocks_in_final_position);
    }

    #[test]
    fn test_move_box_in_target_zone() {
        let (game, mut map) = setup_tests();
        let old_position = Position { x: 0, y: 0 };
        let new_position = Position { x: 1, y: 0 };
        map.movable_blocks_in_final_position = 1;
        map.map[0][1] = MapTile::TargetZone;
        game.calc_nof_blocks_in_target_position(&mut map, &old_position, &new_position);
        assert_eq!(1, map.movable_blocks_in_final_position);
    }

    #[test]
    fn test_has_won() {
        let (game, mut map) = setup_tests();
        map.movable_blocks.push(MovableBlock {
            position: Position { x: 0, y: 0 },
        });
        map.movable_blocks_in_final_position = 1;
        assert!(game.check_has_won(&map));
    }

    #[test]
    fn test_has_not_won() {
        let (game, mut map) = setup_tests();
        map.movable_blocks.push(MovableBlock {
            position: Position { x: 0, y: 0 },
        });
        assert!(!game.check_has_won(&map));
    }

    #[test]
    fn test_get_current_map() {
        let (game, _) = setup_tests();
        let mut equal_map = Map::default();
        equal_map.map[0][0] = MapTile::TargetZone;
        assert_eq!(equal_map, game.get_current_map());
    }
}
