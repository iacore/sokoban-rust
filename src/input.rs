use crate::movement::MoveDirection;

#[derive(Debug, Eq, PartialEq)]
pub enum GameCommand {
    Reset,
    Quit,
    NextMap,
    PreviousMap,
}

#[derive(Debug, Eq, PartialEq)]
pub enum InputAction {
    /// player movement
    Movement(MoveDirection),
    /// game control
    Control(GameCommand),
}

pub trait InputProvider {
    fn next_input_action(&mut self) -> Option<InputAction>;
}
