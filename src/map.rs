pub const MAX_MAP_DIM: usize = 30;

use crate::movement::{calc_new_position_after_movement, MoveDirection};

#[derive(Debug, Clone, Eq, PartialEq, Default)]
pub struct Map {
    pub map: [[MapTile; MAX_MAP_DIM]; MAX_MAP_DIM],
    pub player_position: Position,
    pub movable_blocks: Vec<MovableBlock>,
    pub movable_blocks_in_final_position: usize,
    pub id: usize,
}

#[derive(Debug, Copy, Clone, Eq, PartialEq, Default)]
pub enum MapTile {
    #[default]
    Space,
    Wall,
    Block,
    TargetZone,
}

#[derive(Debug, Copy, Clone, Eq, PartialEq, Default)]
pub struct Position {
    pub x: i32,
    pub y: i32,
}
#[derive(Debug, Clone, Eq, PartialEq)]
pub struct MovableBlock {
    pub position: Position,
}

impl MovableBlock {
    pub fn move_to(&mut self, move_direction: &MoveDirection) {
        self.position = calc_new_position_after_movement(move_direction, &self.position);
    }
}

impl Map {
    pub fn parse_single_line(&mut self, line: &str, line_idx: usize) {
        for (idx, c) in line.chars().enumerate() {
            let mut tile = MapTile::Space;
            match c {
                'X' => tile = MapTile::Wall,
                '*' => self.movable_blocks.push(MovableBlock {
                    position: Position {
                        y: line_idx as i32,
                        x: idx as i32,
                    },
                }),
                '.' => tile = MapTile::TargetZone,
                '@' => {
                    self.player_position = Position {
                        x: idx as i32,
                        y: line_idx as i32,
                    }
                }
                ' ' => (),
                _ => (),
            }
            self.map[line_idx][idx] = tile;
        }
    }

    pub fn parse_map_block(&mut self, input_map_block: &[&str]) {
        for (line_idx, line) in input_map_block.iter().enumerate() {
            self.parse_single_line(line, line_idx);
        }
    }

    pub fn is_movable_block_at(&self, position: &Position) -> bool {
        for block in self.movable_blocks.iter() {
            if block.position == *position {
                return true;
            }
        }
        false
    }

    pub fn get_movable_block_at(&mut self, position: &Position) -> Option<&mut MovableBlock> {
        self.movable_blocks
            .iter_mut()
            .find(|block| block.position == *position)
    }

    pub fn get_tile_type_for_position(&self, position: &Position) -> MapTile {
        self.map[position.y as usize][position.x as usize]
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_get_new_map() {
        let map_to_test = Map::default();
        assert!(map_to_test.movable_blocks.is_empty());
        assert_eq!(0, map_to_test.movable_blocks_in_final_position);
        assert_eq!(0, map_to_test.id);
        for y in 0..MAX_MAP_DIM {
            for x in 0..MAX_MAP_DIM {
                assert_eq!(
                    MapTile::Space,
                    map_to_test.get_tile_type_for_position(&Position {
                        x: x as i32,
                        y: y as i32
                    })
                );
            }
        }
    }

    #[test]
    fn test_is_movable_block_at() {
        let mut map = Map::default();
        map.movable_blocks.push(MovableBlock {
            position: Position { x: 5, y: 5 },
        });
        assert!(map.is_movable_block_at(&Position { x: 5, y: 5 }));
    }

    #[test]
    fn test_parse_map_block() {
        let mut map = Map::default();
        let first_line = String::from(" X@*.");
        let second_line = String::from("X");
        let block_input = vec![first_line.as_str(), second_line.as_str()];
        map.parse_map_block(&block_input);

        assert!(map.is_movable_block_at(&Position { x: 3, y: 0 }));
        assert_eq!(
            MapTile::Space,
            map.get_tile_type_for_position(&Position { x: 0, y: 0 })
        );
        assert_eq!(
            MapTile::Wall,
            map.get_tile_type_for_position(&Position { x: 1, y: 0 })
        );
        assert_eq!(
            MapTile::Wall,
            map.get_tile_type_for_position(&Position { x: 0, y: 1 })
        );
        assert_eq!(
            MapTile::TargetZone,
            map.get_tile_type_for_position(&Position { x: 4, y: 0 })
        );
        assert_eq!(Position { x: 2, y: 0 }, map.player_position);
    }

    #[test]
    fn test_move_movable_block() {
        let mut block = MovableBlock {
            position: Position { x: 5, y: 6 },
        };
        block.move_to(&MoveDirection::Up);
        assert_eq!(Position { x: 5, y: 5 }, block.position);
    }

    #[test]
    fn test_get_movable_block_at_sucess() {
        let mut map = Map::default();
        let block = MovableBlock {
            position: Position { y: 7, x: 5 },
        };
        map.movable_blocks.push(block);
        let result = map.get_movable_block_at(&Position { y: 7, x: 5 });
        assert_eq!(
            result.unwrap(),
            &MovableBlock {
                position: Position { y: 7, x: 5 }
            }
        );
    }
    #[test]
    fn test_get_movable_block_at_failure() {
        let mut map = Map::default();
        let result = map.get_movable_block_at(&Position { x: 0, y: 0 });
        assert!(result.is_none());
    }
}

use std::io;

pub trait MapLoader {
    fn get_maps(&self) -> Result<String, io::Error>;
}

#[derive(Default)]
pub struct MapManager {
    pub maps: Vec<Map>,
}

impl MapManager {
    pub fn load_maps(&mut self, loader: &mut dyn MapLoader) -> Result<(), io::Error> {
        self.read_maps(loader)?;
        Ok(())
    }
    fn read_maps(&mut self, map_content_provider: &mut dyn MapLoader) -> Result<(), io::Error> {
        use regex::Regex;

        let regex_mapcontent = Regex::new(r"^[ X]+[ X*@\.&]+").unwrap();
        let regex_divider = Regex::new(r"^\*+").unwrap();

        let map_contents = map_content_provider.get_maps()?;
        let mut currentmap = Map::default();
        let mut map_block: Vec<&str> = Vec::new();
        for line in map_contents.lines() {
            if regex_mapcontent.is_match(line) {
                map_block.push(line);
            } else if regex_divider.is_match(line) {
                currentmap.parse_map_block(&map_block);
                currentmap.id = self.maps.len();
                self.maps.push(currentmap);
                currentmap = Map::default();
                map_block.clear();
            }
        }
        Ok(())
    }
}

#[cfg(test)]
mod tests_manager {
    use super::*;

    pub struct FakeMapContentProvider {}

    impl MapLoader for FakeMapContentProvider {
        fn get_maps(&self) -> Result<String, io::Error> {
            Ok(String::from(
                "blablabla\n\
        blabla\n\
         XX\n\
        *************************************\n\
         XX\n\
        ******",
            ))
        }
    }

    #[test]
    fn test_map_manager() {
        let mut map_manager = MapManager { maps: Vec::new() };
        let result = map_manager.read_maps(&mut FakeMapContentProvider {});
        assert!(result.is_ok());
        assert_eq!(2, map_manager.maps.len());
        assert_eq!(1, map_manager.maps[1].id);
    }
}
